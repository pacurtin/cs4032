class Client

  #Run the provided echo.php first using php -S localhost:8000 -t

  require 'socket'      # Sockets are in standard library

  hostname = 'localhost'
  port = 8000

  s = TCPSocket.open(hostname, port)
  s.puts "KILL_SERVER"
                        #Send the word test as a message

  while line = s.gets   # Read lines from the socket
    puts line.chop      # Prints what was recieved from socket

  end
  s.close               # Close the socket when done

end