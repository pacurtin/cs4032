class Client2

  require 'socket'      # Sockets are in standard library

  hostname = 'localhost'
  port = 8000

  s = TCPSocket.open(hostname, port)
  s.puts "HELO"

  while line = s.gets   # Read lines from the socket
    puts line.chop      # Prints what was received from socket
  end
  s.close               # Close the socket when done

end